package de.kaiser.dennis.implementation;

import java.awt.BasicStroke;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;

import de.kaiser.dennis.entities.Entity;
import de.kaiser.dennis.entities.Fish;
import de.kaiser.dennis.entities.Shark;
import de.kaiser.dennis.graph.Graph;
import de.kaiser.dennis.keyListener.KeyHandler;
import de.kaiser.dennis.map.Map;

public class Viewer extends Canvas {	

	private static final long           serialVersionUID = 1L;
	private static 		 boolean        displayStatus;	
	private static       JFrame         frame;
    private static       BufferStrategy bs;
    private	static	     Controller		controller;
    private static       int            HEIGHT;
    private static       int            WIDTH;
    private static       Graphics2D     g2D;
		
	public Viewer(int width, int height, Controller controller) {
		super();
		Viewer.WIDTH = width;
		Viewer.HEIGHT = height;
		Viewer.controller = controller;
		setFrame(this.createFrame(width, height));
		addKeyListener(new KeyHandler(this, Viewer.controller));
		requestFocus();
		
        createBufferStrategy(2);
        bs = getBufferStrategy();
        g2D = (Graphics2D)bs.getDrawGraphics();
	}
	
	private JFrame createFrame(int width, int height) {
		JFrame frame = new JFrame();
		Dimension size = new Dimension(width, height);
		frame = new JFrame();
		frame.setPreferredSize(size);
		frame.setUndecorated(true);
		frame.setResizable(false);
		frame.setTitle("Wator");
		frame.setBackground(Color.BLACK);
		frame.add(this);
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);		
		return frame;
	}
	
	public void render (Map map, int scale, Graph fishGraph, Graph sharkGraph) {        
        //Renders all entities
        renderEntities(map, scale, g2D);	
		
        //Renders the stats
		if(displayStatus) {
			renderStats(g2D, map, fishGraph, sharkGraph);  
			renderGraphs(g2D, fishGraph, sharkGraph);
		}
		bs.show();
	}
	
	public void closeFrame() {
		frame.dispose();
	}
	
	private void renderEntities(Map map, int scale, Graphics2D g2D) {
		for(int y = 0; y < map.getHeight(); y++) {
			for(int x = 0; x < map.getWidth(); x++) {
				Entity e = map.getEntity(x, y);
				if(e != null) {
					if(e instanceof Fish) {
			            g2D.setColor(Color.GREEN);
			            g2D.fillRect(x * scale , y * scale, scale, scale);
					}
					if(e instanceof Shark) {
			            g2D.setColor(Color.BLUE);
			            g2D.fillRect(x * scale , y * scale, scale, scale);
					}
				} else {
					
					g2D.setColor(Color.BLACK);
					g2D.fillRect(x * scale, y * scale , scale, scale);
					
//					if((x % 2 == 0) && (y % 2 == 0))
//						g2D.setColor(Color.BLACK);
//					else
//						g2D.setColor(Color.WHITE);
//		            g2D.fillRect(x * scale , y * scale, scale, scale);
//		            g2D.drawString(x +"|" +(y - 1), x * scale, y * scale);
//		            g2D.setColor(Color.BLACK);
//		            g2D.drawRect(x * scale , y * scale, scale, scale);
				}
			}
		}
	}
	
	public void renderEnd() {
		g2D.setColor(Color.ORANGE);
		g2D.fillRect(0, 0, Viewer.WIDTH, Viewer.HEIGHT);
		bs.show();
	}
	
	private void renderStats(Graphics2D g2D, Map map, Graph fishGraph, Graph sharkGraph) {
		String fish  = "Fish: " +fishGraph.getRawMaxYValue() +" Steps: " +fishGraph.getRawLastX();//+map.getFishCount();
		String shark = "Shark: " +sharkGraph.getRawMaxYValue();//+map.getSharkCount();
		Font f = new Font("Plain", Font.PLAIN, 21);
		g2D.setFont(f);
		Rectangle2D sharkRect = g2D.getFontMetrics().getStringBounds(shark, g2D);
		Rectangle2D fishRect  = g2D.getFontMetrics().getStringBounds(fish, g2D);
		Rectangle2D background;
		if(sharkRect.getWidth() > fishRect.getWidth()) {
			background = sharkRect;				
		} else {
			background = fishRect;
		}
		g2D.setColor(Color.BLACK);
		g2D.fillRect(0, 0,(int) background.getWidth(),(int)(sharkRect.getHeight()+fishRect.getHeight()));
		g2D.setColor(Color.WHITE);
		int offset = 20;
		g2D.drawString(fish, 0,offset);
		g2D.drawString(shark, 0, (int) fishRect.getHeight() + offset);
		
		
		
		//g2D.drawRect(0, Viewer.HEIGHT - fishGraph.getMaxYValue(), 100, 100);
		//g2D.drawLine(0, sharkGraph.getMaxYValue(), Viewer.WIDTH, sharkGraph.getMaxYValue());
		
	}
	
	private void renderGraphs(Graphics2D g2D, Graph fishGraph, Graph sharkGraph) {			
		//Max Values for Y
		int maxYValueFish  = fishGraph.getMaxYValue();
		int maxYValueShark = sharkGraph.getMaxYValue();
		
		//Max X Value (always the same for fish and shark
		int maxXValue = fishGraph.getLastX();
		
		//Determine higher Y Value to draw
		g2D.setColor(Color.BLACK);
		if(maxYValueFish > maxYValueShark) {
			g2D.fillRect(0, Viewer.HEIGHT - maxYValueFish, maxXValue, maxYValueFish);
		} else {
			g2D.fillRect(0, Viewer.HEIGHT - maxYValueShark, maxXValue, maxYValueShark);
		}
		
		//sets the stroke thickness
		g2D.setStroke(new BasicStroke(5, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER));
		//tyl3r yin yang : 21.04.15 Jonas Ohland
		//Draws the individual graphs
		g2D.setColor(Color.GREEN);
		g2D.drawPolyline(fishGraph.getXPoints(), fishGraph.getYPoints(), fishGraph.getXPoints().length);
		g2D.setColor(Color.BLUE);
		g2D.drawPolyline(sharkGraph.getXPoints(), sharkGraph.getYPoints(), sharkGraph.getXPoints().length);
	}
	
	public void setDisplayStatus() {
		if(displayStatus)
			displayStatus = false;
		else
			displayStatus = true;
	}

	public static JFrame getFrame() {
		return frame;
	}

	public static void setFrame(JFrame frame) {
		Viewer.frame = frame;
	}
}
