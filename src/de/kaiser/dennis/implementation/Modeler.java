package de.kaiser.dennis.implementation;

import de.kaiser.dennis.entities.Shark;
import de.kaiser.dennis.map.Map;

public class Modeler {
	private int      width;
	private int      height;
	
	private boolean  gameOver;
	
	private Map map;

	public Modeler(int width, int height, int amtFish, int amtShark, 
			       int energyShark, int breedingThresholdFish, int breedingThresholdShark) {
		this.width                  = width;
		this.height                 = height;
		this.gameOver               = true;
		this.map                    = new Map(width, height);
		this.map.init();
		this.map.fillWithRandom(amtFish, amtShark, breedingThresholdFish, breedingThresholdShark, energyShark);
	}
	
	//triggers the update of every entity on the map
	public void updateGame() {
		//If the game is not over it is okay to update the game
		if (!gameOver) {
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					if (this.map.getEntity(x, y) != null) {
						this.map.getEntity(x, y).update(this.map);
					}
				}
			}
			this.gameOver = this.checkGameOver();
		}
	}
	
	private boolean checkGameOver() {
		boolean onlyFish = true;
		boolean everythingEmpty = true;
		for(int y = 0; y < height; y++) {
			for(int x = 0; x < width; x++) {
				if(this.map.getEntity(x, y) != null) {
					everythingEmpty = false;
					if(this.map.getEntity(x, y) instanceof Shark) 
						onlyFish = false;
				}
			}
		}
		return everythingEmpty || onlyFish;
	}
	
	//Resets the moved token on every entity
	public void resetMovedToken() {
		for(int y = 0; y < height; y++) {
			for(int x = 0; x < width; x++) {
				if(this.map.getEntity(x, y) != null) {
					this.map.getEntity(x, y).resetMove();
				}
			}
		}
	}
	
	public void startGame() {
		this.gameOver = false;
	}
	
	public void stopGame() {
		this.gameOver = true;
	}
	
	public boolean isGameOver() {
		return this.gameOver;
	}
	
	 public Map getMap() {
		return this.map;
	}
	
	public int getHeight() {
		return this.height;
	}
	
	public int getWidth() {
		return this.width;
	}	
}
