package de.kaiser.dennis.implementation;

import de.kaiser.dennis.graph.Graph;


public class Controller {
	public  int    width  = 1920;
	public  int    height = width / 16 * 9;
	public  int    scale  = 4;	
	public  int    graphHeightScale = 200;
	public  int    graphWidthScale = 4;
	
	private int amtFish   = 3000;
	private int amtSharks = 800;
	private int energySharks = 5;
	private int breedingTresholdFish = 2;
	private int breedingTresholdSharks = 1000;
	
	private boolean closeGamePending = false;
	private boolean isPaused = false;
	
	private Modeler modeler;
	private Viewer  viewer;
	
	private Graph fishGraph;
	private Graph sharkGraph;
	
	public Controller() {
		this.modeler    = new Modeler(width / scale, height / scale, amtFish, amtSharks, energySharks, breedingTresholdFish, breedingTresholdSharks);			
		this.viewer     = new Viewer(width, height, this);
		this.fishGraph  = new Graph(this.height, this.graphHeightScale, this.graphWidthScale);
		this.sharkGraph = new Graph(this.height, this.graphHeightScale, this.graphWidthScale);
	}
	
	public void start() {
		modeler.startGame();
		while(!modeler.isGameOver() && !closeGamePending) {
			if (!isPaused) {
				triggerRender();
				triggerUpdate();
			}
		}
		viewer.renderEnd();
		if(this.closeGamePending)
			viewer.closeFrame();
	}
	
	public void triggerUpdate() {
		modeler.updateGame();
		modeler.resetMovedToken();
	}
	
	public void triggerRender() {
		fishGraph.addPoint(modeler.getMap().getFishCount());
		sharkGraph.addPoint(modeler.getMap().getSharkCount());
		viewer.render(modeler.getMap(), scale, fishGraph, sharkGraph);		
	}
	
	public void closeGame() {
		this.closeGamePending = true;
	}
	
	public void pauseGame() {
		this.isPaused = !this.isPaused;
	}

}
