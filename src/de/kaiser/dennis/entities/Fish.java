package de.kaiser.dennis.entities;

import java.awt.Point;

import de.kaiser.dennis.map.Map;

public class Fish extends Entity {

	public Fish(int x, int y, int energy, int breedingThreshold) {
		super(x, y, energy, breedingThreshold);
	}

	@Override
	protected void move(int moveID, Map map) {
		Point p = getNextDestCoordinate(moveID, map);
		map.move(this.x, this.y, p.x, p.y);
		this.x = p.x;
		this.y = p.y;
		this.energy++;
	}
	
	@Override
	protected Entity getNewEntity(int destID, Map map) {
		return new Fish(
				this.getNewXByDestID(destID, map),
				this.getNewYByDestID(destID, map), 0,
				this.breedingThreshold);
	}
	
	@Override
	protected void breed(Map map) {
		if(this.energy >= this.breedingThreshold) {
			//Zuf�llig einen nachkommen erzeugen
			int nextDestID = this.getNextDestID(map);
			if(nextDestID != -1) {
				Point p = this.getNextDestCoordinate(nextDestID, map);
				map.add(p.x, p.y, this.getNewEntity(nextDestID, map));
			}
			
			//Eigene Energie auf 0 setzen
			this.energy = 0;			
		}
	}
	
	@Override
	protected boolean isValidNeighbour(Entity e) {
		return e == null ? true : false;
	}
	
	public String toString() {
		return "F";
	}

}
