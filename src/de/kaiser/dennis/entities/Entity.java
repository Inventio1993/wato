package de.kaiser.dennis.entities;

import java.awt.Point;
import java.util.Random;

import de.kaiser.dennis.map.Map;

public abstract class Entity {
	protected int x;
	protected int y;
	protected int energy;
	protected int breedingThreshold;
	
	protected boolean moved;
	
	public Entity(int x, int y, int energy, int breedingThreshold) {
		this.x                 = x;
		this.y                 = y;
		this.energy            = energy;
		this.breedingThreshold = breedingThreshold;		
		this.moved             = false;
	}
	
	public void update(Map map) {
		if (!this.moved) {
			int nextDestID = getNextDestID(map);
			if (nextDestID != -1) {
				this.move(nextDestID, map);
			}
			this.breed(map);
			this.moved = true;
		}
	}
	
	public void resetMove() {
		this.moved = false;
	}
	
	public int getEnergy() {
		return this.energy;
	}
	
	//generates a random number between min and max (inclusively)
	protected int randInt(int min, int max) {
		Random rand = new Random();
	    return rand.nextInt((max - min) + 1) + min;
	}
	
	protected Point getNextDestCoordinate(int destID, Map map) {
		return new Point(getNewXByDestID(destID, map), getNewYByDestID(destID, map));
	}
	
	protected int getNewXByDestID(int destID, Map map) {
		switch(destID) {
			case 2: {
				if(this.x + 1 >= map.getWidth())
					return 0;
				else
					return this.x + 1;
			}
			case 4: {
				if(this.x - 1 < 0)
					return map.getWidth() -1;
				else
					return this.x - 1;
			}
			default: {
				return this.x;
			}
		}
	}
	
	protected int getNewYByDestID(int destID, Map map) {
		switch(destID) {
			case 1: {
				if(this.y - 1 < 0)
					return map.getHeight() - 1;
				else
					return this.y - 1;
			}
			case 3: {
				if(this.y + 1 >= map.getHeight())
					return 0;
				else
					return this.y + 1;					
			}
			default: {
				return this.y;
			}
		}
	}
	
	protected int getNextDestID(Map map) {
		boolean[] validNeighbours = this.getValidNeighbours(map);
		int rndNr = this.randInt(0, this.getTrueCount(validNeighbours));
		int countFoundFields = 0;
		for(int i = 0; i < 4; i++) {
			if(validNeighbours[i] == true)
				countFoundFields++;
			if(countFoundFields == rndNr) {
				//found the next field
				return i;
			}
		}
		return -1;
	}
	
	protected int getTrueCount(boolean[] validNeighbours) {
		int out = 0;
		for(int i = 0; i < validNeighbours.length; i++) {
			if(validNeighbours[i] == true)
				out++;
		}
		return out;
	}
	
//	protected void breed(Map map) {
//		if(this.energy >= this.breedingThreshold) {
//			int nextDestID = getNextDestID(map);
//			if (nextDestID != -1) {
//				Point p = getNextDestCoordinate(nextDestID, map);
//				map.add(p.x, p.y, this.getNewEntity(nextDestID, map));
//			}
//		}
//	}	
	
	protected boolean[] getValidNeighbours(Map map) {
		boolean[] out = new boolean[4];
		for(int i = 0; i < 4; i++) {
			Entity e = map.getEntity(this.getNewXByDestID(i, map), this.getNewYByDestID(i, map));
			if(this.isValidNeighbour(e)) 
				out[i] = true;
			else 
				out[i] = false;
		}
		return out;
	}	
	
	protected abstract boolean isValidNeighbour(Entity e);
	protected abstract void move(int moveID, Map map);
	protected abstract void breed(Map map);
	protected abstract Entity getNewEntity(int destID, Map maps);
}
