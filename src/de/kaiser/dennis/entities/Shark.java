package de.kaiser.dennis.entities;

import java.awt.Point;

import de.kaiser.dennis.map.Map;

public class Shark extends Entity {
	
	private int initialEnergy;

	public Shark(int x, int y, int energy, int breedingThreshold, int initialEnergy) {
		super(x, y, energy, breedingThreshold);
		this.initialEnergy = initialEnergy;
	}
	
	@Override
	protected void move(int moveID, Map map) {
		if(this.energy <= 0) {
			//Shark death
			map.remove(this.x, this.y);
			return;
		}
		Point p = getNextDestCoordinate(moveID, map);
		if(map.getEntity(p.x, p.y) instanceof Fish) {
			this.energy += map.getEntity(p.x, p.y).getEnergy();
		}
		map.move(this.x, this.y, p.x, p.y);
		this.x = p.x;
		this.y = p.y;
		this.energy--;
	}
	
//	protected void breed(Map map) {
//	if(this.energy >= this.breedingThreshold) {
//		int nextDestID = getNextDestID(map);
//		if (nextDestID != -1) {
//			Point p = getNextDestCoordinate(nextDestID, map);
//			map.add(p.x, p.y, this.getNewEntity(nextDestID, map));
//		}
//	}
//}		
	
	@Override
	protected void breed(Map map) {
		if(this.energy >= this.breedingThreshold) {
			//Shark is ready to breed
			//Breed a new shark with half of the energy of the parent
			int nextDestID = getNextDestID(map);
			if(nextDestID != -1) {
				Point p = getNextDestCoordinate(nextDestID, map); //get coords of the new shark
				map.add(p.x, p.y, this.getNewEntity(nextDestID, map)); //create new shark at the new coords
				this.energy = this.initialEnergy;
			}
		}
	}
	
	
	@Override
	protected Entity getNewEntity(int destID, Map map) {
		int tempEnergy = (this.energy / 2);
		tempEnergy++;
		return new Shark(this.getNewXByDestID(destID, map),
				  this.getNewYByDestID(destID, map), tempEnergy,
				  this.breedingThreshold, this.initialEnergy);
	}
	
	@Override
	protected boolean isValidNeighbour(Entity e) {
		return (e == null || e instanceof Fish) ? true : false;
	}
	
	public String toString() {
		return "S";
	}

}
