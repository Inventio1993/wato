package de.kaiser.dennis.graph;

import java.awt.Point;
import java.util.ArrayList;

public class Graph {
	private ArrayList<Point> points;
	private int lastX;
	private int lastY;
	private int windowHeight;
	private int heightScale;
	private int widthScale;
	
	public Graph(int windowHeight, int heightScale, int widthScale) {
		this.points = new ArrayList<Point>();
		this.lastX = 0;		
		this.windowHeight = windowHeight;
		this.heightScale = heightScale;
		this.widthScale = widthScale;
	}
	
	public void addPoint(int value) {
		//points.add(new Point(++lastX * 4, windowHeight - (value / 200)));
		points.add(new Point(++lastX, value));
		this.lastY = value;
	}
	
	public ArrayList<Point> getPoints() {
		return this.points;
	}
	
	public int[] getXPoints() {
		int[] temp = new int[this.points.size()];
		for(int i = 0; i < points.size(); i++) {
			temp[i] = points.get(i).x * 4;
		}
		return temp;
	}
	
	public int[] getYPoints() {
		int[] temp = new int[this.points.size()];
		for(int i = 0; i < points.size(); i++) {
			temp[i] = windowHeight - (points.get(i).y / this.heightScale);
		}
		return temp;
	}
	
	public int getMinYValue() {
		int temp = Integer.MAX_VALUE;
		for(Point p: points) {
			if(p.y < temp) {
				temp = p.y;
			}
		}
		return temp / this.heightScale;
	}
	
	public int getMaxYValue() {
		int temp = 0;
		for(Point p: points) {
			if(p.y > temp) {
				temp = p.y;
			}
		}
		return temp / this.heightScale;
	}
	
	public int getRawMinYValue() {
		int temp = Integer.MAX_VALUE;
		for(Point p: points) {
			if(p.y < temp) {
				temp = p.y;
			}
		}
		return temp;
	}
	
	public int getRawMaxYValue() {
		int temp = 0;
		for(Point p: points) {
			if(p.y > temp) {
				temp = p.y;
			}
		}
		return temp;
	}
	
	public int getLastX() {
		return this.lastX * this.widthScale;
	}
	
	public int getRawLastX() {
		return this.lastX;
	}

}
