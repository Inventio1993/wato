package de.kaiser.dennis.keyListener;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import de.kaiser.dennis.implementation.Controller;
import de.kaiser.dennis.implementation.Viewer;

public class KeyHandler implements KeyListener {
	
	private Viewer viewer;
	private Controller controller;
	
	public KeyHandler(Viewer viewer, Controller controller) {
		this.controller = controller;
		this.viewer = viewer;
	}
	

	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_D) {
			viewer.setDisplayStatus();
		}
		
		if(e.getKeyCode() == KeyEvent.VK_U) {
			controller.triggerUpdate();
		}
		
		if(e.getKeyCode() == KeyEvent.VK_R) {
			controller.triggerRender();
		}
		
		if(e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			controller.closeGame();
		}
		
		if(e.getKeyCode() == KeyEvent.VK_P) {
			controller.pauseGame();
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

}
