package de.kaiser.dennis.main;

import de.kaiser.dennis.implementation.Controller;

public class Main {

	public static void main(String[] args) {
		Controller controller = new Controller();
		controller.start();
	}

}
