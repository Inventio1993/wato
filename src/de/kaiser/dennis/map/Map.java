package de.kaiser.dennis.map;

import java.util.Random;

import de.kaiser.dennis.entities.Entity;
import de.kaiser.dennis.entities.Fish;
import de.kaiser.dennis.entities.Shark;

public class Map {
	private int width;
	private int height;
	private Entity[][] map;

	public Map(int width, int height) {
		this.width  = width;
		this.height = height;
		this.map = new Entity[width][height];
	}
	
	public void init() {
		for(int y = 0; y < this.height; y++) {
			for(int x = 0; x < this.width; x++) {
				this.map[x][y] = null;
			}
		}
	}
	
	public void fillWithRandom(int amtFish, int amtShark, int breedingThresholdFish, int breedingThresholdShark, int energyShark) {
		for(int i = 0; i < amtFish; i++) {
			boolean success = false;
			while(!success) {
				int x = randInt(0, this.width -1 );
				int y = randInt(0, this.height - 1);
				if(this.map[x][y] == null) {
					this.map[x][y] = new Fish(x, y, 0, breedingThresholdFish);
					success = true;
				}
			}
		}
		
		for(int i = 0; i < amtShark; i++) {
			boolean success = false;
			while(!success) {
				int x = randInt(0, this.width -1 );
				int y = randInt(0, this.height - 1);
				if(this.map[x][y] == null) {
					this.map[x][y] = new Shark(x, y, energyShark, breedingThresholdFish, energyShark);
					success = true;
				}
			}
		}
	}
	
	//generates a random number between min and max (inclusively)
	public int randInt(int min, int max) {
		Random rand = new Random();
	    return rand.nextInt((max - min) + 1) + min;
	}
	
	public void add(int x, int y, Entity e) {
		this.map[x][y] = e;
	}
	
	public void remove(int x, int y) {
		this.map[x][y] = null;
	}
	
	public void move(int x, int y, int newX, int newY) {
		if(x != newX || y != newY) {			
			this.map[newX][newY] = this.map[x][y];
			this.map[x][y] = null;
		}
	}
	
	/*
	 * Getter
	 */
	public int getWidth() {
		return this.width;
	}

	public int getHeight() {
		return this.height;
	}

	public Entity[][] getMap() {
		return this.map;
	}
	
	public Entity getEntity(int x, int y) {
		return this.map[x][y];
	}
	
	public int getSharkCount() {
		int count = 0;
		for(int y = 0; y < height; y++) {
			for(int x = 0; x < width; x++) {
				if(map[x][y] instanceof Shark)
					count++;
			}
		}
		return count;
	}
	
	public int getFishCount() {
		int count = 0;
		for(int y = 0; y < height; y++) {
			for(int x = 0; x < width; x++) {
				if(map[x][y] instanceof Fish)
					count++;
			}
		}
		return count;		
	}
	
}
